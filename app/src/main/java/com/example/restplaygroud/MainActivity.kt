package com.example.restplaygroud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.restplaygroud.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

//KESINI HBS KE LAYOUT DAN INTERFACE
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val webService = ProductWebService.builder()
        binding.btnGetProduct.setOnClickListener {
            getProduct(webService)
        }
    }

    //2. Buat kelas get product sampai call back dipanggil
    private fun getProduct(webService: ProductWebService) {
        val callback = object : Callback<ProductResponse>{
            override fun onResponse(
                call: Call<ProductResponse>,
                response: Response<ProductResponse>
            ) {
               val body = response.body()

                //bikin ini
                binding.tvProduct.text = body?.title
            }

            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                //untuk cek erorr logging
                t.printStackTrace()
            }

        }
        webService.getProduct()
            .enqueue(callback)


    }

}