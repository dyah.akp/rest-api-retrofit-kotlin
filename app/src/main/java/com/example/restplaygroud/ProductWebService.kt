package com.example.restplaygroud

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductWebService {  //0 bikin interface
    //https://fakestoreapi.com/products/1
    //base url com
    //end point adalah = products
    //1 = path

    //2https://fakestoreapi.com/products
    @GET("/products/1")
    fun getProduct(): Call<ProductResponse> //1

    //ccd
    @GET("/products/{id}")
    fun getSingleProduct(@Path(value = "id") id : Int): Call<ProductResponse>

//b 1.
    @GET("/products")
    fun getProducts(): Call<List<ProductResponse>>

    //3 bikin function
    companion object {
        fun builder(): ProductWebService {
            //5 buat variable converter
            val gsonConverterFactory = GsonConverterFactory.create() //dipanggil di add converter factory dan build

                //8
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY


            //7
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()

            //buat implement 4 val sampe base url sampai add factory
            val retrofit = Retrofit.Builder()
                .baseUrl("https://fakestoreapi.com/")
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .build()

            //6 buat variable product web service
            val productWebService = retrofit.create(ProductWebService::class.java)
            return  productWebService

        }
    }

}