package com.example.restplaygroud

import ProductAdapter
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.restplaygroud.databinding.ActivityProductsBinding
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint  //ini panggil
class ProductActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProductsBinding

    private val productAdapter = ProductAdapter()

    @Inject  //ini panggil
    //blm di define,
  lateinit var productWebService: ProductWebService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecylerView()
        //setup api

//        val productWebService = ProductWebService.builder()
        val callback = object : Callback<List<ProductResponse>> {
            override fun onResponse(
                call: Call<List<ProductResponse>>,
                response: Response<List<ProductResponse>>
            ) {
                val body = response.body()
                if (body != null) {
                    //setelah
                    binding.pbProducts.isVisible = false
                    productAdapter.addNewProducts(body)
                }
            }

            override fun onFailure(call: Call<List<ProductResponse>>, t: Throwable) {
                t.printStackTrace()
            }

        }

        productWebService.getProducts().enqueue(callback)

    }

    private fun setupRecylerView() {
        binding.rvProducts.layoutManager = LinearLayoutManager(this)
        binding.rvProducts.adapter = productAdapter
        //U 6 sampe start intent, terus ke detail activity
            productAdapter.onClickProduct {
                val intent = Intent(applicationContext, ProductDetailActivity::class.java)
                intent.putExtra(KEY_ID, it.id)
                startActivity(intent)
            }

    }

    //U 5 bikin object
    companion object {
        const val  KEY_ID = "id"
    }
}