package com.example.restplaygroud

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.restplaygroud.ProductActivity.Companion.KEY_ID
import com.example.restplaygroud.databinding.ActivityDetailProductBinding
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class ProductDetailActivity : AppCompatActivity(){
    private lateinit var binding: ActivityDetailProductBinding

    @Inject
    lateinit var productWebService: ProductWebService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailProductBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val id = intent.getIntExtra(KEY_ID, 0)
        val callback = object : Callback<ProductResponse>{
            override fun onResponse(
                call: Call<ProductResponse>,
                response: Response<ProductResponse>
            ) {
                val body = response.body()
                if ( body !=null){
                    Glide.with(binding.root.context)
                        .load(body.image)
                        .circleCrop()
                        .into(binding.ivDetailImage)
                    binding.tvDetailTitleProduct.text = body.title
                    binding.tvCategoryProduct.text = body.category
                    binding.tvDescriptionProduct.text = body.description
                    binding.tvPriceProduct.text = body.price.toString()
                }
            }

            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
             //ini
                t.printStackTrace()
            }

        }

        productWebService.getSingleProduct(id).enqueue(callback)

    }

}