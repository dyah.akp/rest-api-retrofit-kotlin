import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.restplaygroud.ProductResponse
import com.example.restplaygroud.databinding.ItemProductsBinding

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ProductsViewHolder>() {

//U 1 bikin var
    private var onClick : (ProductResponse) -> Unit={}  // M 1 bikin button
    private var products: MutableList<ProductResponse> = mutableListOf()

    inner class ProductsViewHolder(
        private val binding: ItemProductsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        //2
        fun bind(item: ProductResponse) {

            binding.tvProductName.text = item.title
            binding.tvProductAmount.text = "Rp${item.price}"

            Glide.with(binding.root.context)
                .load(item.image)
                .circleCrop()
                .into(binding.imgProducts)


            //U 2 bikin binding
            binding.clProduct.setOnClickListener {
                onClick(item)
            }
            //U 3 ke bawah 4

        }
    }


    //U4 bikin function  , setelah itu ke activity
    fun onClickProduct(clickProduct: (ProductResponse) -> Unit ){
        onClick = clickProduct
    }

    fun addNewProducts(newProducts: List<ProductResponse>) {
        products.addAll(newProducts)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemProductsBinding.inflate(inflater, parent, false)
        val viewHolder = ProductsViewHolder(binding)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        val product = products[position]
        holder.bind(product)

    }

    override fun getItemCount(): Int {
        return products.size
    }
}

